/*
 * This file is part of FancyGradle, licensed under the MIT License
 *
 * Copyright (c) 2023 Garden of Fancy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

@file:JvmName("0__DependencyHandlerExt_Internal_")

package wtf.gofancy.fancygradle.internal

import org.gradle.api.Action
import org.gradle.api.artifacts.ExternalModuleDependency
import org.gradle.api.artifacts.dsl.DependencyHandler
import java.nio.file.Path

internal fun DependencyHandler.createExternal(group: String, name: String, version: String? = null, configuration: String? = null, classifier: String? = null,
                                              ext: String? = null, dependencyConfiguration: Action<ExternalModuleDependency>? = null): ExternalModuleDependency {
    return (this.create(makeDependencyMap(group, name, version, configuration, classifier, ext)) as? ExternalModuleDependency)?.also { dependencyConfiguration?.execute(it) }
        ?: throw IllegalStateException()
}

internal fun makeDependencyString(group: String, name: String, version: String? = null, classifier: String? = null, ext: String? = null) =
    buildString {
        this.append(group)
        this.append(':')
        this.append(name)
        if (version != null) this.append(':').append(version)
        if (classifier != null) this.append(':').append(classifier)
        if (ext != null) this.append('@').append(ext)
    }

private fun makeDependencyMap(group: String, name: String, version: String? = null, configuration: String? = null,
                              classifier: String? = null, ext: String? = null) =
    mapOfFilteringNullValues(
        "group" to group,
        "name" to name,
        "version" to version,
        "configuration" to configuration,
        "classifier" to classifier,
        "ext" to ext
    )
