/*
 * This file is part of FancyGradle, licensed under the MIT License
 *
 * Copyright (c) 2023 Garden of Fancy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

@file:JvmName("0__MCPCHelp_Internal_")

package wtf.gofancy.fancygradle.internal

import net.minecraftforge.gradle.common.util.HashFunction
import net.minecraftforge.gradle.common.util.MavenArtifactDownloader
import net.minecraftforge.gradle.common.util.Utils
import net.minecraftforge.gradle.mcp.util.MCPWrapper
import net.minecraftforge.gradle.userdev.UserDevExtension
import org.gradle.api.Project
import org.gradle.api.artifacts.ExternalModuleDependency
import java.nio.file.Path

private const val MCP_CONFIG_GROUP = "de.oceanlabs.mcp"
private const val MCP_CONFIG_NAME = "mcp_config"
private const val MCP_REPO = "mcp_repo"
private const val FORGE_GROUP = "net.minecraftforge"
private const val FORGE_NAME = "forge"

internal val Project.minecraftExtension get() = this.extensions.getByName(UserDevExtension.EXTENSION_NAME) as UserDevExtension

internal fun findMcVersion(project: Project): String? {
    val minecraftConfiguration = project.configurations.findByName("minecraft") ?: return null
    val probablyMcDep = minecraftConfiguration.dependencies.find { it.group == FORGE_GROUP && it.name == FORGE_NAME && it is ExternalModuleDependency }
    val mcDep = probablyMcDep as? ExternalModuleDependency ?: return null

    val fullBlownVersion = mcDep.version ?: return null
    if (!fullBlownVersion.contains("mapped")) return fullBlownVersion

    val forgeVersion = fullBlownVersion.substring(0, fullBlownVersion.indexOf("_mapped"))
    if (forgeVersion.contains('_')) return null // WTF?
    if (!forgeVersion.contains('-')) return forgeVersion

    return forgeVersion.split('-')[0]
}

internal fun findMcpConfig(project: Project, version: String): Path? =
    MavenArtifactDownloader.manual(project, makeDependencyString(group = MCP_CONFIG_GROUP, name = MCP_CONFIG_NAME, version = version, ext = "zip"), false)
        ?.toPath()

internal fun makeMcpWrapper(project: Project, data: Path, version: String): MCPWrapper =
    data.toFile().let { file -> MCPWrapper(HashFunction.SHA1.hash(file), file, findCachedMcpConfig(project, version).toFile()) }

internal fun findCachedMcpConfig(project: Project, version: String): Path =
    Utils.getCache(project, MCP_REPO)
        .toPath()
        .resolveDependencyDirectory(group = MCP_CONFIG_GROUP, name = MCP_CONFIG_NAME, version = version)
