/*
 * This file is part of FancyGradle, licensed under the MIT License
 *
 * Copyright (c) 2023 Garden of Fancy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

@file:JvmName("0__GClosureMan_Internal_")

package wtf.gofancy.fancygradle.internal

import groovy.lang.Closure

private class KClosure0<R>(val action: () -> R, owner: Any?, `this`: Any?) : Closure<R>(owner, `this`) {
    @Suppress("UNUSED") fun doCall(): R? = this.action()
}

private class KClosure1<in P, R>(val action: (P) -> R, owner: Any?, `this`: Any?) : Closure<R>(owner, `this`) {
    @Suppress("UNUSED") fun doCall(it: P): R? = this.action(it)
}

private class KClosure2<in P, in Q, R>(val action: (P, Q) -> R, owner: Any?, `this`: Any?) : Closure<R>(owner, `this`) {
    @Suppress("UNUSED") fun doCall(one: P, two: Q): R? = this.action(one, two)
}

private class KClosure3<in P, in Q, in T, R>(val action: (P, Q, T) -> R, owner: Any?, `this`: Any?) : Closure<R>(owner, `this`) {
    @Suppress("UNUSED") fun doCall(one: P, two: Q, three: T): R? = this.action(one, two, three)
}

private class KClosure4<in P, in Q, in T, in S, R>(val action: (P, Q, T, S) -> R, owner: Any?, `this`: Any?) : Closure<R>(owner, `this`) {
    @Suppress("UNUSED") fun doCall(one: P, two: Q, three: T, four: S): R? = this.action(one, two, three, four)
}

internal fun <R> Any.closureOf(action: () -> R): Closure<R> = KClosure0(action, this, this)
internal fun <P, R> Any.closureOf(action: (P) -> R): Closure<R> = KClosure1(action, this, this)
internal fun <P, Q, R> Any.closureOf(action: (P, Q) -> R): Closure<R> = KClosure2(action, this, this)
internal fun <P, Q, T, R> Any.closureOf(action: (P, Q, T) -> R): Closure<R> = KClosure3(action, this, this)
internal fun <P, Q, T, S, R> Any.closureOf(action: (P, Q, T, S) -> R): Closure<R> = KClosure4(action, this, this)

