/*
 * This file is part of FancyGradle, licensed under the MIT License
 *
 * Copyright (c) 2023 Garden of Fancy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

@file:JvmName("0__IntRunConfigUtils_Internal_")

package wtf.gofancy.fancygradle.internal

import net.minecraftforge.gradle.userdev.UserDevExtension
import org.gradle.api.tasks.TaskContainer
import java.io.File

// Internal for now, may expose later
internal fun UserDevExtension.runConfigProperty(key: String, value: File) {
    this.runs.forEach {
        it.property(key, value)
    }
}

internal fun TaskContainer.prepareRunDependsOn(vararg paths: Any) {
    this.filter {
        it.name.startsWith("prepareRun") && it.name != "prepareRun"
    }.forEach {
        it.dependsOn(*paths)
    }
}
