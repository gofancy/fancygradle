/*
 * This file is part of FancyGradle, licensed under the MIT License
 *
 * Copyright (c) 2023 Garden of Fancy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package wtf.gofancy.fancygradle.patch.coremods

import org.gradle.api.Project
import wtf.gofancy.fancygradle.internal.minecraftExtension
import wtf.gofancy.fancygradle.internal.runtimeClasspath
import wtf.gofancy.fancygradle.patch.PatchExecutor
import wtf.gofancy.fancygradle.patch.common.ExtractMappingsZipTask
import wtf.gofancy.fancygradle.patch.common.MappingsPatchExecutor
import java.net.URI

internal class CoremodsPatchExecutor : MappingsPatchExecutor() {
    private companion object {
        private const val LEGACY_DEV_GROUP = "net.minecraftforge"
        private const val LEGACY_DEV_NAME = "legacydev"
        private const val LEGACY_DEV_VERSION_PATCH = "0.2.4.+"
    }

    override fun applyPatch(project: Project) {
        super.applyPatch(project)

        project.repositories.maven {
            it.name = "Su5eD LegacyDev Fork"
            it.url = URI("https://maven.su5ed.dev/releases")
        }

        project.configurations.runtimeClasspath {
            resolutionStrategy { strategy ->
                strategy.eachDependency { dep ->
                    if (dep.requested.group == LEGACY_DEV_GROUP && dep.requested.name == LEGACY_DEV_NAME) {
                        dep.useVersion(LEGACY_DEV_VERSION_PATCH)
                        dep.because("Fixes coremods and ATs")
                    }
                }
            }
        }
    }
}
