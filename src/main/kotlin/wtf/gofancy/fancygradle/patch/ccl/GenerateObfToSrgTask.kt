/*
 * This file is part of FancyGradle, licensed under the MIT License
 *
 * Copyright (c) 2023 Garden of Fancy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package wtf.gofancy.fancygradle.patch.ccl

import net.minecraftforge.srgutils.IMappingFile
import org.gradle.api.DefaultTask
import org.gradle.api.file.RegularFileProperty
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.OutputFile
import org.gradle.api.tasks.TaskAction
import wtf.gofancy.fancygradle.internal.FancyTask
import wtf.gofancy.fancygradle.internal.findMcVersion
import wtf.gofancy.fancygradle.internal.findMcpConfig
import wtf.gofancy.fancygradle.internal.makeMcpWrapper
import java.io.ByteArrayInputStream
import java.io.File

open class GenerateObfToSrgTask : DefaultTask(), FancyTask {
    companion object {
        private const val MAPPINGS_DATA_PATH = "mappings"
    }

    @get:Input
    val version: Property<String> = this.project
        .objects
        .property(String::class.java)
        .convention(this.project.provider { findMcVersion(this.project) ?: throw IllegalStateException("No Minecraft found") })

    @get:OutputFile
    val outputFile: RegularFileProperty = this.project
        .objects
        .fileProperty()
        .convention(this.project.layout.buildDirectory.file("${this.name}${File.separator}obfToSrg.srg"))

    @TaskAction
    override fun act() {
        val mcpConfig = findMcpConfig(this.project, this.version.get()) ?: throw IllegalStateException("No MCP found")
        val wrapper = makeMcpWrapper(this.project, mcpConfig, this.version.get())
        val data = wrapper.getData(MAPPINGS_DATA_PATH)
        val mappingFile = IMappingFile.load(ByteArrayInputStream(data))
        mappingFile.write(this.outputFile.get().asFile.toPath(), IMappingFile.Format.SRG, false)
    }
}
