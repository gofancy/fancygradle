/*
 * This file is part of FancyGradle, licensed under the MIT License
 *
 * Copyright (c) 2023 Garden of Fancy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package wtf.gofancy.fancygradle.patch.common

import net.minecraftforge.gradle.common.util.MavenArtifactDownloader
import net.minecraftforge.gradle.mcp.MCPRepo
import org.gradle.api.DefaultTask
import org.gradle.api.file.DirectoryProperty
import org.gradle.api.file.RegularFileProperty
import org.gradle.api.tasks.InputFile
import org.gradle.api.tasks.OutputDirectory
import org.gradle.api.tasks.TaskAction
import wtf.gofancy.fancygradle.internal.FancyTask
import wtf.gofancy.fancygradle.internal.minecraftExtension

open class ExtractMappingsZipTask : DefaultTask(), FancyTask {
    @get:InputFile
    val inputFile: RegularFileProperty = this.project
        .objects
        .fileProperty()
        .convention {
            val dep = MCPRepo.getMappingDep(this.project.minecraftExtension.mappingChannel.get(), this.project.minecraftExtension.mappingVersion.get())
            MavenArtifactDownloader.generate(this.project, dep, false)!!
        }

    @get:OutputDirectory
    val outputDirectory: DirectoryProperty = this.project
        .objects
        .directoryProperty()
        .convention(this.project.layout.buildDirectory.dir(this.name))

    @TaskAction
    override fun act() {
        this.outputDirectory.get().asFile.let {
            it.deleteRecursively()
            it.mkdirs()
        }

        this.project.copy {
            it.from(this.project.zipTree(this.inputFile.asFile))
            it.into(this.outputDirectory)
        }
    }
}
