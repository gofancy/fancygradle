/*
 * This file is part of FancyGradle, licensed under the MIT License
 *
 * Copyright (c) 2023 Garden of Fancy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package wtf.gofancy.fancygradle.patch.resources

import net.minecraftforge.gradle.common.util.RunConfig
import org.gradle.api.Project
import org.gradle.plugins.ide.idea.model.IdeaModel
import wtf.gofancy.fancygradle.internal.minecraftExtension
import wtf.gofancy.fancygradle.patch.PatchExecutor

internal class ResourcesPatchExecutor : PatchExecutor {
    override fun applyPatch(project: Project) {
        val modifyClassPathTask = project.tasks.register("patchModifyClassPath", ModifyClassPathTask::class.java)

        modifyClassPathTask.configure {
            it.group = PatchExecutor.TASK_GROUP
            it.dependsOn("jar")
        }

        val runs = project.minecraftExtension.runs.map(RunConfig::getTaskName)

        project.tasks.whenTaskAdded { task ->
            if (runs.contains(task.name)) {
                task.dependsOn(modifyClassPathTask)
            }
        }

        project.pluginManager.withPlugin("idea") {
            // Configuring idea, or trying at least
            try {
                project.extensions.getByType(IdeaModel::class.java).module.inheritOutputDirs = true
            } catch (ignore: NoClassDefFoundError) {
            } catch (ignore: ClassNotFoundException) {
            }
        }
    }
}
