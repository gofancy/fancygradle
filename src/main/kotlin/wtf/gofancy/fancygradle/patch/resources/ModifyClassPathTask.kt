/*
 * This file is part of FancyGradle, licensed under the MIT License
 *
 * Copyright (c) 2023 Garden of Fancy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package wtf.gofancy.fancygradle.patch.resources

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.JavaExec
import org.gradle.api.tasks.TaskAction
import org.gradle.api.tasks.bundling.Jar
import wtf.gofancy.fancygradle.internal.FancyTask
import wtf.gofancy.fancygradle.internal.minecraftExtension
import wtf.gofancy.fancygradle.internal.runtimeClasspath

open class ModifyClassPathTask : DefaultTask(), FancyTask {

    @TaskAction
    override fun act() {
        val jar = project.tasks.getByName("jar") as Jar
        val classpath = project.files(project.configurations.runtimeClasspath, jar.archiveFile.get().asFile)
        project.minecraftExtension.runs
                .map { project.tasks.getByName(it.taskName) }
                .filterIsInstance<JavaExec>()
                .forEach { it.classpath = classpath }
    }
}
