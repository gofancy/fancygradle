/*
 * This file is part of FancyGradle, licensed under the MIT License
 *
 * Copyright (c) 2023 Garden of Fancy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package wtf.gofancy.fancygradle.patch

import wtf.gofancy.fancygradle.patch.asm.AsmPatchExecutor
import wtf.gofancy.fancygradle.patch.ccl.CclPatchExecutor
import wtf.gofancy.fancygradle.patch.resources.ResourcesPatchExecutor
import wtf.gofancy.fancygradle.patch.coremods.CoremodsPatchExecutor

@Deprecated(message = "Use the standalone properties instead", level = DeprecationLevel.WARNING)
enum class Patch(internal val executor: PatchExecutor) {
    CODE_CHICKEN_LIB(CclPatchExecutor()),
    RESOURCES(ResourcesPatchExecutor()),
    @Suppress("SpellCheckingInspection") COREMODS(CoremodsPatchExecutor()),
    ASM(AsmPatchExecutor())
}
