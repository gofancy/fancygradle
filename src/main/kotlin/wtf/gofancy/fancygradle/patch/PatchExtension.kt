/*
 * This file is part of FancyGradle, licensed under the MIT License
 *
 * Copyright (c) 2023 Garden of Fancy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package wtf.gofancy.fancygradle.patch

import org.gradle.api.Project
import org.gradle.api.provider.ListProperty
import wtf.gofancy.fancygradle.patch.asm.AsmPatchExecutor
import wtf.gofancy.fancygradle.patch.ccl.CclPatchExecutor
import wtf.gofancy.fancygradle.patch.coremods.CoremodsPatchExecutor
import wtf.gofancy.fancygradle.patch.mergetool.MergetoolPatchExecutor
import wtf.gofancy.fancygradle.patch.resources.ResourcesPatchExecutor

class PatchExtension(private val project: Project) {
    private val objectFactory = project.objects

    internal val patches: ListProperty<PatchExecutor> = this.objectFactory.listProperty(PatchExecutor::class.java)

    val asm get() = this.patch(AsmPatchExecutor())
    val codeChickenLib get() = this.patch(CclPatchExecutor())
    @Suppress("SpellCheckingInspection") val coremods get() = this.patch(CoremodsPatchExecutor())
    val resources get() = this.patch(ResourcesPatchExecutor())
    val mergetool get() = this.patch(MergetoolPatchExecutor())

    @Deprecated(message = "Use the single properties for patches instead", replaceWith = ReplaceWith(""), level = DeprecationLevel.ERROR)
    fun patch(vararg patch: Patch) {
        this.patches.addAll(patch.map { it.executor })
    }

    private fun patch(patch: PatchExecutor) {
        this.patches.add(patch)
    }
}
