/*
 * This file is part of FancyGradle, licensed under the MIT License
 *
 * Copyright (c) 2023 Garden of Fancy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package wtf.gofancy.fancygradle

import org.gradle.api.Plugin
import org.gradle.api.Project
import wtf.gofancy.fancygradle.internal.withForgeGradle
import wtf.gofancy.fancygradle.script.groovy.injectExtensions

class FancyGradle : Plugin<Project> {
    override fun apply(target: Project) {
        val fancyExtension = target.extensions.create(FancyExtension.EXTENSION_NAME, FancyExtension::class.java, target)

        target.pluginManager.withForgeGradle {
            target.injectExtensions()

            target.afterEvaluate {
                fancyExtension.applyPatches(it)
            }
        }
    }

    private fun FancyExtension.applyPatches(project: Project) {
        this.patches.patches.get().forEach { it(project) }
    }
}
