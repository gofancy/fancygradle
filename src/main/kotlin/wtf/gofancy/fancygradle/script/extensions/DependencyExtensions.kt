/*
 * This file is part of FancyGradle, licensed under the MIT License
 *
 * Copyright (c) 2023 Garden of Fancy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

@file:JvmName("DependencyExt")

package wtf.gofancy.fancygradle.script.extensions

import net.minecraftforge.gradle.userdev.DependencyManagementExtension
import org.gradle.api.Project
import org.gradle.api.artifacts.Dependency
import org.gradle.api.artifacts.ExternalModuleDependency
import org.gradle.api.artifacts.dsl.RepositoryHandler
import org.gradle.api.artifacts.repositories.MavenArtifactRepository
import wtf.gofancy.fancygradle.internal.createExternal
import wtf.gofancy.fancygradle.internal.reflect
import java.net.URI

private const val CURSE_MAVEN_LINK = "https://cursemaven.com"
private const val CURSE_MAVEN_GROUP = "curse.maven"

private val dependencyManagementExtensionProject by lazy { DependencyManagementExtension::class.reflect().propertyOfType<Project>() ?: throw IllegalStateException() }
private val DependencyManagementExtension.project: Project get() = dependencyManagementExtensionProject(this)

fun RepositoryHandler.curseForge(action: MavenArtifactRepository.() -> Unit = {}) : MavenArtifactRepository {
    return this.maven {
        it.name = "CurseForge"
        it.url = URI(CURSE_MAVEN_LINK)

        it.content { descriptor ->
            descriptor.includeGroup(CURSE_MAVEN_GROUP)
        }

        action(it)
    }
}

fun Project.curse(mod: String, projectId: Long, fileId: Long): Dependency {
    require(projectId >= 0) { "The project ID '$projectId' for mod '$mod' is negative." }
    require(fileId >= 0) { "The file ID '$fileId' for mod '$mod' is negative." }
    return this.dependencies.createExternal(group = CURSE_MAVEN_GROUP, name = "$mod-$projectId", version = fileId.toString())
}

@Suppress("SpellCheckingInspection")
fun DependencyManagementExtension.deobf(group: String, name: String, version: String? = null, configuration: String? = null,
                                        classifier: String? = null, ext: String? = null, dependencyConfiguration: ((ExternalModuleDependency) -> Unit)? = null): ExternalModuleDependency {
    return this.deobf(project.dependencies.createExternal(group, name, version, configuration, classifier, ext, dependencyConfiguration)) as ExternalModuleDependency
}
