/*
 * This file is part of FancyGradle, licensed under the MIT License
 *
 * Copyright (c) 2023 Garden of Fancy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

@file:JvmName("RunConfigExt")

package wtf.gofancy.fancygradle.script.extensions

import net.minecraftforge.gradle.common.util.RunConfig
import org.gradle.api.NamedDomainObjectContainer
import org.gradle.api.Project
import org.gradle.api.tasks.SourceSet
import wtf.gofancy.fancygradle.internal.reflect
import java.util.Locale

private val runConfigProject by lazy { RunConfig::class.reflect().propertyOfType<Project>() ?: throw IllegalStateException() }
private val RunConfig.project: Project get() = runConfigProject(this)

object CommonRunConfigurationSettings {
    private val debugLogging: RunConfig.() -> Unit = { this.property("forge.logging.console.level", "debug") }
    private val forgeDefaults: RunConfig.() -> Unit = {
        this.property("forge.logging.markers", "REGISTRIES")
        this.debugLogging()
    }

    fun debugLogging(): RunConfig.() -> Unit = this.debugLogging

    fun forgeDefaults(): RunConfig.() -> Unit = this.forgeDefaults

    fun dataGenerationDefaults(modId: String): RunConfig.() -> Unit = {
        args("--mod", modId, "--all", "--output", this.project.file("src/generated/resources"), "--existing", this.project.file("src/main/resources"))
    }
}

fun NamedDomainObjectContainer<RunConfig>.createRunConfig(name: String, modId: String? = null, vararg sourceSets: SourceSet, config: RunConfig.() -> Unit = {}): RunConfig {
    require(name.isNotBlank()) { "Invalid empty name for run config." }
    require(name.lowercase(Locale.ENGLISH) == name) { "Run config name '${name}' must be lowercase." }

    if (modId != null) {
        require(modId.lowercase(Locale.ENGLISH) == modId) { "Mod ID '${modId}' for run config must be lowercase." }
        require(sourceSets.isNotEmpty()) { "Run configuration with a mod must specify at least one source set." }
    }

    return this.create(name) {
        it.workingDirectory(it.project.file("run_$name"))
        it.config()
        if (modId != null) {
            it.mods.create(modId) { config -> sourceSets.forEach { set -> config.source(set) } }
        }
    }
}

fun NamedDomainObjectContainer<RunConfig>.createDataGenerationRunConfig(name: String, modId: String, vararg sourceSets: SourceSet, config: RunConfig.() -> Unit = {}): RunConfig {
    return this.createRunConfig(name, modId, *sourceSets) {
        CommonRunConfigurationSettings.dataGenerationDefaults(modId)(this)
        this.config()
    }
}

fun NamedDomainObjectContainer<RunConfig>.createDebugLoggingRunConfig(name: String, modId: String? = null, vararg sourceSets: SourceSet, config: RunConfig.() -> Unit = {}): RunConfig {
    return this.createRunConfig(name, modId, *sourceSets) {
        CommonRunConfigurationSettings.debugLogging()(this)
        this.config()
    }
}

fun NamedDomainObjectContainer<RunConfig>.createDefaultMdkRunConfig(name: String, modId: String? = null, vararg sourceSets: SourceSet, config: RunConfig.() -> Unit = {}): RunConfig {
    return this.createRunConfig(name, modId, *sourceSets) {
        CommonRunConfigurationSettings.forgeDefaults()(this)
        this.config()
    }
}
