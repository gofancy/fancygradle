/*
 * This file is part of FancyGradle, licensed under the MIT License
 *
 * Copyright (c) 2023 Garden of Fancy
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

@file:JvmName("__GSEI")

package wtf.gofancy.fancygradle.script.groovy

import groovy.lang.Closure
import net.minecraftforge.gradle.common.util.RunConfig
import org.gradle.api.NamedDomainObjectContainer
import org.gradle.api.Project
import org.gradle.api.artifacts.Dependency
import org.gradle.api.artifacts.repositories.MavenArtifactRepository
import org.gradle.api.plugins.ExtensionAware
import org.gradle.api.plugins.ExtraPropertiesExtension
import org.gradle.api.tasks.SourceSet
import wtf.gofancy.fancygradle.internal.closureOf
import wtf.gofancy.fancygradle.internal.minecraftExtension
import wtf.gofancy.fancygradle.script.extensions.createDataGenerationRunConfig
import wtf.gofancy.fancygradle.script.extensions.createDebugLoggingRunConfig
import wtf.gofancy.fancygradle.script.extensions.createDefaultMdkRunConfig
import wtf.gofancy.fancygradle.script.extensions.createRunConfig
import wtf.gofancy.fancygradle.script.extensions.curse
import wtf.gofancy.fancygradle.script.extensions.curseForge

internal fun Project.injectExtensions() {
    this.injectDependencyExtensions()
    this.injectRunConfigExtensions()
}

private fun Project.injectDependencyExtensions() {
    this.extend {
        this["curse"] = closureOf<String, Long, Long, Dependency> { mod, pId, fId ->
            this@injectDependencyExtensions.curse(mod, pId, fId)
        }
    }

    this.repositories.extend {
        this["curseForge"] = closureOf<Closure<*>?, MavenArtifactRepository> {
            project.repositories.curseForge { it?.call(this) }
        }
    }
}

private fun Project.injectRunConfigExtensions() = this.minecraftExtension.runs.injectRunConfigExtensions()

private fun NamedDomainObjectContainer<RunConfig>.injectRunConfigExtensions() {
    fun ExtraPropertiesExtension.makeExt(name: String, call: (String, String?, Array<SourceSet>, RunConfig.() -> Unit) -> RunConfig) {
        this[name] = closureOf<String, String?, Array<SourceSet>?, Closure<*>?, RunConfig> { confName, modId, sets, conf ->
            call(confName, modId, sets ?: arrayOf()) { conf?.call(this) }
        }
    }

    this.extend {
        this.makeExt("createRunConfig", this@injectRunConfigExtensions::createRunConfig)
        this.makeExt("createDataGenerationRunConfig") { name, modId, sets, config ->
            this@injectRunConfigExtensions.createDataGenerationRunConfig(name, modId!!, *sets, config = config)
        }
        this.makeExt("createDebugLoggingRunConfig", this@injectRunConfigExtensions::createDebugLoggingRunConfig)
        this.makeExt("createDefaultMdkRunConfig", this@injectRunConfigExtensions::createDefaultMdkRunConfig)
    }
}

private fun Any.extend(fail: Boolean = true, block: ExtraPropertiesExtension.() -> Unit) {
    if ((this as? ExtensionAware)?.extend(block) == null && fail) {
        throw IllegalStateException("Attempted to extend a non-ExtensionAware object: something failed elsewhere")
    }
}

private fun ExtensionAware.extend(block: ExtraPropertiesExtension.() -> Unit) = block(this.extensions.extraProperties)
