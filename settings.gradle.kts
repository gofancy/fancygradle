pluginManagement {
    repositories {
        gradlePluginPortal()
        maven {
            name = "Garden of Fancy"
            url = uri("https://maven.gofancy.wtf/releases")
        }
    }
}

rootProject.name = "FancyGradle"
