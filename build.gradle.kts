import fr.brouillard.oss.gradle.plugins.JGitverPluginExtensionBranchPolicy
import fr.brouillard.oss.jgitver.Strategies
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import java.time.Instant
import java.time.format.DateTimeFormatter
import java.util.*

plugins {
    `java-gradle-plugin`
    `maven-publish`
    kotlin("jvm") version "1.5.0"
    id("com.github.ben-manes.versions") version "0.38.0"
    id("org.cadixdev.licenser") version "0.6.1"
    id("org.jetbrains.dokka") version "1.4.20"
    id("com.gradle.plugin-publish") version "0.14.0"
    id("fr.brouillard.oss.gradle.jgitver") version "0.10.+"
}

pluginBundle {
    website = "https://gitlab.com/gofancy/fancygradle"
    vcsUrl = "https://gitlab.com/gofancy/fancygradle"
    tags = listOf("minecraft", "modding")
}

gradlePlugin {
    plugins {
        create("fancygradle") {
            id = "wtf.gofancy.fancygradle"
            displayName = "FancyGradle"
            description = "A Gradle plugin to ease minecraft mod development and other menial Gradle tasks"
            implementationClass = "wtf.gofancy.fancygradle.FancyGradle"
        }
    }
}

java {
    toolchain.languageVersion.set(JavaLanguageVersion.of(JavaVersion.VERSION_1_8.majorVersion))
}

license {
    header(project.file("NOTICE"))

    ext["year"] = Calendar.getInstance().get(Calendar.YEAR)
    ext["name"] = "Garden of Fancy"
    ext["app"] = "FancyGradle"
}

jgitver {
    strategy = Strategies.PATTERN
    versionPattern = "\${M}\${<m}\${<p}\${<meta.COMMIT_DISTANCE}\${-~meta.QUALIFIED_BRANCH_NAME}"
    
    policy(closureOf<JGitverPluginExtensionBranchPolicy> {
        pattern = "(develop/.*)"
        transformations = mutableListOf("IGNORE")
    })
}

group = "wtf.gofancy"

publishing {
    publications {
        create<MavenPublication>("bleeding") {
            from(components["java"])

            // in order for gradle to find the plugin (otherwise a resolution strategy would need to be used)
            groupId = "wtf.gofancy.fancygradle"
            artifactId = "wtf.gofancy.fancygradle.gradle.plugin"
            version = project.version.toString()
        }

        repositories {
            if (project.hasProperty("gofancy_maven_user") && project.hasProperty("gofancy_maven_secret")) {
                maven {
                    name = "gofancy"
                    url = uri("https://maven.gofancy.wtf/releases")

                    credentials {
                        username = project.property("gofancy_maven_user")!!.toString()
                        password = project.property("gofancy_maven_secret")!!.toString()
                    }
                }
            }
        }
    }
}

repositories {
    mavenCentral()
    maven {
        name = "Minecraft Forge"
        url = uri("https://maven.minecraftforge.net/")
    }
}

dependencies {
    compileOnly(group = "net.minecraftforge.gradle", name = "ForgeGradle", version = "5.1.+")
    compileOnly(group = "net.minecraftforge", name = "artifactural", version = "2.0.3")
    compileOnly(group = "net.minecraftforge", name = "srgutils", version = "0.4.3")
    
    implementation(kotlin("reflect"))
    implementation(kotlin("stdlib-jdk8"))

    testImplementation(group = "org.assertj", name = "assertj-core", version = "3.19.0")
    testImplementation(group = "org.junit.jupiter", name = "junit-jupiter", version = "5.7.1")
}

tasks {
    dokkaGfm.configure {
        outputDirectory.set(buildDir.resolve("kdoc"))
        dokkaSourceSets {
            configureEach {
                jdkVersion.set(8)
                skipEmptyPackages.set(false)
                skipDeprecated.set(false)
            }
        }
    }

    withType<KotlinCompile> {
        kotlinOptions {
            jvmTarget = JavaVersion.VERSION_1_8.toString()
            freeCompilerArgs = listOf("-Xjvm-default=all", "-Xlambdas=indy")
        }
    }

    withType<Test> {
        useJUnitPlatform()
        testLogging {
            events("passed", "skipped", "failed")
        }
    }

    withType<Jar> {
        manifest {
            attributes(
                "Name" to "wtf/gofancy/fancygradle",
                "Specification-Title" to "FancyGradle",
                "Specification-Version" to project.version,
                "Specification-Vendor" to "Garden of Fancy",
                "Implementation-Title" to "wtf.gofancy.fancygradle",
                "Implementation-Version" to project.version,
                "Implementation-Vendor" to "Garden of Fancy",
                "Implementation-Timestamp" to DateTimeFormatter.ISO_INSTANT.format(Instant.now())
            )
        }
    }

    withType<Wrapper> {
        gradleVersion = "7.1.1"
        distributionType = Wrapper.DistributionType.ALL
    }
}
